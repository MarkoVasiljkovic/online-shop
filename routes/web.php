<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/product/search','ProductController@search')->name('product.search');
Route::resource('/product','ProductController');
Route::get('/product/image/{image}','ProductController@outputimg')->name('product.outputimg');


Route::resource('/shop','ShopController');
Route::resource('/admin','AdminController');
Route::resource('/productColor','ProductColorController');

Route::get('/cart/checkout', 'CartController@checkout')->name('cart.checkout');
Route::get('/cart', 'CartController@index')->name('cart.index');
Route::get('/cart/{product}', 'CartController@add')->name('cart.add');
Route::get('/cart/destroy/{itemId}', 'CartController@destroy')->name('cart.destroy');
Route::get('/cart/update/{itemId}', 'CartController@update')->name('cart.update');

Route::get('/order/customer/{order}', 'OrderController@customerShow')->name('customer.show');
Route::get('/order/user/{id}', 'OrderController@orders')->name('order.user');
Route::resource('/order','OrderController');

Route::get('user/{user}', 'UserController@edit')->name('user.edit');
Route::put('user/{user}/update', 'UserController@update')->name('user.update');

Route::post('/kontakt','KontaktController@store')->name('kontakt.store');

// Gallery
Route::get('/mladost', function() {
    return view('projects_gallery.mladost');
});
Route::get('/capriolo', function() {
    return view('projects_gallery.capriolo');
});
Route::get('/eldisy', function() {
    return view('projects_gallery.eldisy');
});





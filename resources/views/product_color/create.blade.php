@extends('layouts.admin')

@section('adminContent')

@if( session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
@endif

<h1>New Color</h1>

<div id="form" class="form">
    <form action="{{ route('productColor.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
          @error('name')
            <div class="text-danger">{{ $message }}</div>
          @enderror
        </div>
       
        <button type="submit" class="btn btn-dark">Save</button>
      </form>
</div>
@endsection
        
@extends('layouts.app')

@section('content')

@if( session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
@endif
<div class="container">
<div class="card card-body bg-light mt-5 shadow text-dark">
<h1>Orders</h1>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Order numer</th>
        <th scope="col">Status</th>
        <th scope="col">Customer</th>
        <th scope="col">Total</th>
        <th scope="col">Date</th>
        <th scope="col">Notes</th>
        <th scope="col">Show</th>
        <th scope="col">Delete</th>
      </tr>
    </thead>
    <tbody>
        
        @foreach( $order as $item )
        <tr>
            <th scope="row">{{ $item->id }}</th>

            <td>{{ $item->order_number }}</td>

            <td>{{ $item->status }}</td>

            <td>{{ $item->fullname }}</td>

            <td>€{{ $item->grand_total }}</td>

            <td>{{ $item->created_at }}</td>

            <td>{{ $item->notes }}</td>

            <td>
              <a href="{{ route('customer.show',$item->id)}}">
                <button type="submit" class="btn btn-outline-dark">
                  <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 0 0 1.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0 0 14.828 8a13.133 13.133 0 0 0-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 0 0 1.172 8z"/>
                    <path fill-rule="evenodd" d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
                  </svg>
                </button>
              </a>
            </td>
          <td><form action="{{ route('order.destroy',$item->id)}}" method="post" class="formdelete">
            @csrf
            @method('delete')
            <button type="submit" class="btn btn-outline-danger"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-archive" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M0 2a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1v7.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 12.5V5a1 1 0 0 1-1-1V2zm2 3v7.5A1.5 1.5 0 0 0 3.5 14h9a1.5 1.5 0 0 0 1.5-1.5V5H2zm13-3H1v2h14V2zM5 7.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z"/>
              </svg></button>
              </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
  {{ $order->links('pagination::bootstrap-4') }}
  </div>
</div>
@endsection
        
@extends('layouts.app')

@section('content')
<div class="container">

    <div class="card card-body bg-light mt-5 shadow text-dark">
        <h2>Your Cart</h2>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Color</th>
                    <th>Unit Price</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cartItems as $item)
                    <tr >
                        <td scope="row">
                            {{ $item->name }}
                        </td>
                        <td>
                            <img src="{{ route('product.outputimg','prev_'.$item->associatedModel->image) }}">
                        </td>
                        <td>
                            {{ $item->attributes->color }}
                        </td>
                        <td>€
                            {{ $item->price }}
                        </td>
                        <td>
                            <form action="{{ route('cart.update', $item->id) }}">
                                <input class="quantity" min="1" name="quantity" value="{{ $item->quantity }}" type="number">
                                <button type="submit" class="btn-xs btn-success">Save</button>
                            </form>
                        </td>
                        <td>€
                            {{-- sum of the Item multiplied by its quantity --}}
                            {{Cart::session(auth()->id())->get($item->id)->getPriceSum()}}
                        </td>
                        <td>
                            <button type="button" class="btn btn-outline-danger">
                                <a href="{{ route('cart.destroy', $item->id) }}">Delete</a> 
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="d-flex justify-content-end">
            <h3>Total Price : € {{ \Cart::session(auth()->id())->getTotal() }} </h3>
        </div>

            <a href="{{ route('cart.checkout') }}" class="btn btn-primary" role="button">Proceed to checkout</a>
    </div>      
</div>
@endsection
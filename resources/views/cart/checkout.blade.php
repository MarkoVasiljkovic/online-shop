@extends('layouts.app')

@section('content')
<div class="container">

    <div class="checkout-page p-5 text-white">

            <h2>Checkout Page</h2>

            <h3>Shipping Information</h3>
            
        <form action="{{ route('order.store') }}" method="POST">
            @csrf

            <div class="form-group">
                <label for="fullname">Full Name:</label>
                <input type="text" class="form-control" name="fullname" id="fullname">
            </div>
            
            <div class="form-group">
                <label for="state">State:</label>
                <input type="text" class="form-control" name="state" id="state">
            </div>
            
            <div class="form-group">
                <label for="city">City:</label>
                <input type="text" class="form-control" name="city" id="city">
            </div>
            
            <div class="form-group">
                <label for="address">Address:</label>
                <input type="text" class="form-control" name="address" id="address">
            </div>
            
            <div class="form-group">
                <label for="zipcode">Postcode:</label>
                <input type="text" class="form-control" name="zipcode" id="zipcode">
            </div>
            
            <div class="form-group">
                <label for="phone">Phone:</label>
                <input type="text" class="form-control" name="phone" id="phone">
            </div>

            <div class="form-group">
                <label for="delivery_method">Delivery method:</label>
                <select class="form-control" name="delivery_method" id="delivery_method" type="text">
                    <option selected>own transport</option>
                    <option>home delivery</option>
                </select>
            </div>
            
            <div class="form-group">
            <label for="notes">Notes:</label>
            <textarea class="form-control" name="notes" id="notes" rows="3"></textarea>
            </div>
            <div class="float-left">
                <p class="text-warning">NOTE: Cash on delivery (C.O.D), or payament in the store!</p>
            </div>
            <button type="submit" class="btn btn-primary btn-lg float-right mt-1">Place Order</button>

        </form>
    </div>
</div>
@endsection
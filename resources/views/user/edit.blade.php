@extends('layouts.app')

@section('content')
<div class="container">

    <div class="container mt-5 mb-5 col-10">
        <div class="checkout-page mt-5 p-5 text-white">
    
            <form method="post" action="{{route('user.update', $user->id)}}">
                @csrf
                @method('put')
            
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" value="{{ old('name',$user->name) }}">
                    @error('name')
                      <div class="text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" name="email" value="{{ old('email',$user->email) }}">
                    @error('email')
                      <div class="text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password">
                    @error('password')
                      <div class="text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                <div class="form-group">
                    <label for="password_confirmation">Password confirmation</label>
                    <input type="password" class="form-control" name="password_confirmation">
                  </div>
            
                <button type="submit" class="btn btn-primary btn-lg float-right">Submit</button>
            </form>
    
        </div>
    </div>
</div>
@endsection
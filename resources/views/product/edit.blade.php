@extends('layouts.admin')

@section('adminContent')

@if( session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
@endif

<h1>Edit Product</h1>

<div id="form" class="form">
    <form action="{{ route('product.update',$product->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" name="name" id="name" value="{{ old('name',$product->name) }}">
          @error('name')
            <div class="text-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="description">Description</label>
          <textarea class="form-control" id="description" name="description" rows="3">{{ old('description',$product->description) }}</textarea>
          @error('beschreibung')
            <div class="text-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="price">Price</label>
          <input type="text" class="form-control" name="price" id="price" value="{{ old('price',$product->price) }}">
          @error('name')
            <div class="text-danger">{{ $message }}</div>
          @enderror
        </div>   
        <div class="form-group">
            <label for="image">Image Upload</label>
            <input type="file" class="form-control-file" name="image" id="image" value="{{ old('image',$product->image) }}">
            @error('image')
              <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
          <label for="draft_img">Draft Image Upload</label>
          <input type="file" class="form-control-file" name="draft_img" id="draft_img" value="{{ old('draft_img',$product->draft_img) }}">
          @error('draft_img')
            <div class="text-danger">{{ $message }}</div>
          @enderror
      </div>
        {{-- ********* CARUSEL ********** --}}

        <h2>Carusel Images</h2>

        <div class="input-group hdtuto control-group lst increment p-2" >
            <input type="file" name="filenames[]" class="myfrm form-control">
            <div class="input-group-btn"> 
                <button class="btn btn-success" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
            </div>
          </div>
            @error('filenames')
              <div class="text-danger">{{ $message }}</div>
            @enderror

        <div class="clone hide">
            <div class="hdtuto control-group lst input-group p-2" style="margin-top:10px">
                <input type="file" name="filenames[]" class="myfrm form-control">
                <div class="input-group-btn"> 
                    <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-dark">Submit</button>
    </form>
</div>

{{-- jquery for carusel Images --}}
<script type="text/javascript">
  $(document).ready(function() {
    $(".btn-success").click(function(){ 
        var lsthmtl = $(".clone").html();
        $(".increment").after(lsthmtl);
    });
    $("body").on("click",".btn-danger",function(){ 
        $(this).parents(".hdtuto").remove();
    });
  });
</script>
@endsection
        
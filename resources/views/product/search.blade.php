@extends('layouts.app')

@section('content')

<div class="container mt-5">

  <div class="row row-cols-3 justify-content-center">
    @foreach( $products as $item )


        <div class="col card m-5 card-body text-dark shadow">
          <img src="{{ route('product.outputimg','big_'.$item->image) }}" width="150" class="card-img-top" alt="product image">
          <div class="card-body">
            <h5 class="card-title">{{ $item->name }}</h5>
            <p class="card-text">{{ $item->description }}</p>
          <a href="{{ route('shop.show', $item->id)}}" class="btn btn-primary">Go to Product</a>
          </div>
        </div>

        @endforeach
      </div>
    </div>


@endsection
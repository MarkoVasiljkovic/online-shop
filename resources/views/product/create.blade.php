@extends('layouts.admin')

@section('adminContent')

@if( session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
@endif

<h1 class="text-dark">New Product</h1>

<div id="form" class="form text-dark">
    <form action="{{ route('product.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
          @error('name')
            <div class="text-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="description">Description</label>
          <textarea class="form-control" id="description" name="description" rows="3">{{ old('description') }}</textarea>
          @error('description')
            <div class="text-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="price">Price</label>
          <input type="text" class="form-control" name="price" id="price" value="{{ old('price') }}">
          @error('name')
            <div class="text-danger">{{ $message }}</div>
          @enderror
        </div>       
        <div class="form-group">
            <label for="image">Image Upload</label>
            <input type="file" class="form-control-file" name="image" id="image">
            @error('image')
              <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="draft_img">Draft Image Upload</label>
            <input type="file" class="form-control-file" name="draft_img" id="draft_img">
            @error('draft_img')
              <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        {{-- ********* CARUSEL ********** --}}

        <h2>Carusel Images</h2>

        <div class="input-group hdtuto control-group lst increment p-2" >
            <input type="file" name="filenames[]" class="myfrm form-control">
            <div class="input-group-btn"> 
                <button class="btn btn-success" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
            </div>
            @error('filenames')
               <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="clone hide">
            <div class="hdtuto control-group lst input-group p-2" style="margin-top:10px">
                <input type="file" name="filenames[]" class="myfrm form-control">
                <div class="input-group-btn"> 
                    <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                </div>
            </div>
        </div>
        <div class="col text-right mt-3">
          <button type="submit" class="btn btn-dark btn-lg">Submit</button>
        </div>
      </form>
</div>

{{-- jquery for carusel images --}}
  <script type="text/javascript">
    $(document).ready(function() {
      $(".btn-success").click(function(){ 
          var lsthmtl = $(".clone").html();
          $(".increment").after(lsthmtl);
      });
      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".hdtuto").remove();
      });
    });
  </script>
@endsection
        
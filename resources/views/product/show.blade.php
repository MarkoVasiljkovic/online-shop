@extends('layouts.admin')

@section('adminContent')

<div class="container mt-5">
    {{-- CAROUSEL --}}
  
    <div id="carouselExampleControls" class="carousel slide pb-5" data-ride="carousel">
      <div class="carousel-inner">

        <div class="carousel-item active pb-4">
          <img src="{{ route('product.outputimg','big_'.$product->image) }}" height="400" class="d-block w-100" alt="...">
        </div>

        @foreach ($carusel as $image)
        <div class="carousel-item">
          <img src="{{ route('product.outputimg','big_'.$image->filename) }}" height="400" class="d-block w-100" alt="...">
        </div>
        @endforeach
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
      {{-- PRODUCT DETAILS --}}
  
      <div class="row justify-content-center">
            <div class="col-md-6">  
  
              <div class="col-12 text-center">
                <img src="{{ route('product.outputimg','big_'.$product->draft_img) }}" width="300" class="rounded">
              </div>    
            </div>
            <div class="col-md-6">
              <div class="row">   
              
              <div class="col-md-6">
  
                  
                  <h5>{{ $product->name }}</h5>
                  <p>Created : <br>{{ $product->created_at }}</p>
                  <p>Updated : <br>{{ $product->updated_at }}</p>
                  
                  <p><span class="mr-1">Price: <strong>€{{ $product->price }}</strong></span></p>
                  
                  <p class="pt-1">{{ $product->description }}</p>
                </div>
                
              <div class="col-md-6 float-right">
                <img src="{{asset('images/ral_karta_obradjena.png')}}" height="250" class="d-block w-100" alt="...">
              </div>
            </div>
            <hr>
              
            <div class="container">
                  <div class="row">
                    <div class="col-sm">
  
                      <label for="color">Color:</label>
                      <select class="form-control" name="color" id="color" type="text">
                        <option disabled selected> -- select an option -- </option>

                        @foreach ($color as $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach

                      </select>
                    </div>
                </div>                
              </div>

          </div>
      
        </div>

@endsection
        
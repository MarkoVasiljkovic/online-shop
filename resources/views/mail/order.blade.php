<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
    <body>

        <table>
            <thead>
                <tr>
                    <th scope="row">Customer:</th>
                    <th>{{ $details['fullname'] }}</th>
                </tr>
                </thead>
                <tbody>

                    <tr>
                        <td scope="row">Delivery method:</td>
                        <td>{{ $details['delivery_method'] }}</td>
                    </tr>
                    <tr>
                        <td scope="row">Total:</td>
                        <td>€ {{ $details['total'] }}</td>
                    </tr>
                    <tr>
                        <td scope="row">Item count:</td>
                        <td>{{ $details['item_count'] }}</td>
                    </tr>
                    <tr>
                        <td scope="row">State:</td>
                        <td>{{ $details['state'] }}</td>
                    </tr>
                    <tr>
                        <td scope="row">City:</td>
                        <td>{{ $details['city'] }}</td>
                    </tr>
                    <tr>
                        <td scope="row">Address:</td>
                        <td>{{ $details['address'] }}</td>
                    </tr>
                    <tr>
                        <td scope="row">Zipcode:</td>
                        <td>{{ $details['zipcode'] }}</td>
                    </tr>
                    <tr>
                        <td scope="row">Phone:</td>
                        <td>{{ $details['phone'] }}</td>
                    </tr>
                    <tr>
                        <td scope="row">Notes:</td>
                        <td>{{ $details['notes'] }}</td>
                    </tr>
                </tbody>
        </table>
        
    </body>
</html>
<footer class="footer-copyright text-center py-2 bg-dark text-white mt-auto">
    Made by: <a class="pr-5" href="https://gitlab.com/MarkoVasiljkovic"> Marko Vasiljkovic</a>
    © 2021 copyright Limomont Steel D.O.O   |   All rights reserved 
</footer>
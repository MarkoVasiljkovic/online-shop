@extends('layouts.app')

@section('content')

    <div class="container mt-5">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar admin sidebar-img{{--  img-overflow --}}">
          <div class="sidebar-sticky elements-text">
            <ul class="nav flex-column">
              <li class="nav-item  mt-4">
                <a class="nav-link active" href="{{ route('user.edit', Auth::user()->id) }}">
                  <i class="fa fa-user-circle-o"></i>
                  Account
                </a>
              </li>
              <li class="nav-item  text-dark">
                <a class="nav-link" href="{{ route('order.index') }}">
                  <i class="fa fa-shopping-cart"></i>
                  Orders
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.index') }}">
                  <i class="fa fa-object-group"></i>
                  All Products
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('product.create') }}">
                  <i class="fa fa-plus-square"></i>
                  Create Product
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('product.index') }}">
                  <i class="fa fa-wrench"></i>
                  Product Management
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('productColor.index') }}">
                  <i class="fa fa-wrench"></i>
                  Color Management
                </a>
              </li>
            </ul>

            {{-- <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-white">
              <span>Custumer view</span>
              <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="plus-circle"></span>
              </a>
            </h6>
            <ul class="nav flex-column mb-2">
              <li class="nav-item">
                <a class="nav-link" href="/#home">
                  <span data-feather="file-text"></span>
                  Home
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/#services">
                  <span data-feather="file-text"></span>
                  Services
                </a>
              </li>
              <li class="nav-item">
              <a class="nav-link" href="/#about">
                  <span data-feather="file-text"></span>
                  About us
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/#projekts">
                  <span data-feather="file-text"></span>
                  Our Projects
                </a>
              </li>
              <li class="nav-item">
              <a class="nav-link" href="{{ route('shop.index') }}">
                  <span data-feather="file-text"></span>
                  Our Products
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/#contact">
                  <span data-feather="file-text"></span>
                  Contact
                </a>
              </li>
            </ul> --}}
          </div>
        </nav>
         <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div class="card card-body bg-light mt-5 shadow">

            @yield('adminContent')
            
            </div>
        </main>
    </div>
  </div>

@endsection
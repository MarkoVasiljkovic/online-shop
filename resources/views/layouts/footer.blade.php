<div id="contact" class="mt-5 checkout-page">
    <div class="row mt-5  mx-5 text-white">
      <div class="col-sm-5 lora">
        
        <p><i class="fa fa-map-marker fa-2x"></i>  Sime Pogacarevica 5,<br>  16000 Leskovac, Srbija</p>
        <p><i class="fa fa-mobile fa-2x"></i>  +381 63 1093687 - Sale</p>
        <p><i class="fa fa-mobile fa-2x"></i>  +381 63 8141111 - Commercial</p>
        <p><i class="fa fa-phone fa-2x"></i>  +381 16261251</p>
        <p><i class="fa fa-envelope fa-2x"></i>  batalimako@gmail.com</p>
      </div>

      <div class="col-sm-7 fredoka">
        <p>Contact us and we'll get back to you within 24 hours.</p>
        <form action="{{ route('kontakt.store')}}" method="POST">
          @csrf
            <div class="row">
              <div class="col-sm-6 form-group">
                <label for="name">Name</label>
                <input class="form-control" id="name" name="name" placeholder="Name" type="text" value="{{ old('name') }}">
                @error('name')
                  <div class="text-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="col-sm-6 form-group">
                <label for="email">E-Mail</label>
                <input class="form-control" id="email" name="email" placeholder="Email" type="email" value="{{ old('email') }}">
                @error('email')
                  <div class="text-danger">{{ $message }}</div>
                @enderror
              </div>
            </div>
            <label for="msg">Message</label>
            <textarea class="form-control" id="msg" name="msg" placeholder="Comment" rows="5">{{ old('msg') }}</textarea><br>
            <div class="row">
              <div class="col-sm-12 form-group">

            <button class="btn-lg my-button pull-right" type="submit">Send</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@cannot('adminonly')
    
    <div class="black-bg sticky-top text-white">
        
        <p class="mb-0">
                <i class="fa fa-phone pl-5"></i>  +381 16261251
                <i class="fa fa-envelope pl-5"></i>  batalimako@gmail.com
                <i class="fa fa-map-marker pl-5"></i>  Sime Pogacarevica 5, 16000 Leskovac, Srbija
                <a class="float-right pr-5" href="https://www.facebook.com/limomont.szr"><i class="fa fa-facebook-official"></i>  LimomontSteel</a>
            </p>

    </div>
@endcannot

@can('adminonly')
    
<div class="black-bg sticky-top">
    <p class="mb-0">
        <i class="fa fa-phone pl-5"></i>  +381 16261251
        <i class="fa fa-envelope pl-5"></i>  batalimako@gmail.com
        <i class="fa fa-map-marker pl-5"></i>  Sime Pogacarevica 5, 16000 Leskovac, Srbija
    </p>
</div>
@endcan
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark shadow-sm" id="grad1">
    {{-- <nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav"> --}}
    {{-- <nav class="navbar navbar-dark sticky-top navbar-expand-lg bg-dark flex-md-nowrap p-0"> --}}
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <a class="navbar-brand brand-text" href="{{ url('/') }}">
                    {{ config('app.name','LIMOMONTSTEEL') }}
                </a>
                {{-- <!-- Left Side Of Navbar --> --}}
                @can('adminonly') 
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin.index')}}">
                                <i class="fa fa-cog fa-spin" style="font-size:24px"></i>
                            </i>
                            Dashboard
                        </a>
                    </li>
                </ul>
                @endcan

            {{-- Search bar --}}
            <form class="form-inline my-5 my-lg-0" action="{{route('product.search')}}" method="GET">
{{--             <input class="form-control mr-sm-4" type="text" name="searchText" placeholder="Enter Your key word">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> --}}
            <div class="container h-100">
                <div class="d-flex justify-content-center h-100"> 
                    <div class="searchbar">
                        <input class="search_input" type="text" name="searchText" placeholder="Search...">
                        <button type="submit" role="button" class="search_icon"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            </form>
 
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                @can('customeronly') 
                <li class="nav-item">
                    <a class="nav-link nav-text" href="{{ route('order.user', Auth::user()->id) }}">
                        <i class="fa fa-list-alt fa-lg" style="color:rgb(216, 226, 25)"></i>
                        Orders
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-text" href="{{ route('cart.index') }}">
                        <i class="fa fa-shopping-cart fa-lg" style="color:rgb(255, 166, 0)"></i>
                        Cart
                        <sup class="badge badge-warning">{{Cart::session(auth()->id())->getContent()->count()}}</sup>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-text" href="{{ route('user.edit', Auth::user()->id) }}">
                        <i class="fa fa-user fa-lg" style="color:#1E90FF"></i>
                        Account
                    </a>
                </li>
                @endcan
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        {{-- <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a> --}}
                        <a class="nav-link nav-text" style="cursor: pointer" data-toggle="modal" data-target="#loginModal">{{ __('Login') }}</a>
                    </li>
                    {{-- ****** REGISTER ***** --}}
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link nav-text" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle nav-text" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                        
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
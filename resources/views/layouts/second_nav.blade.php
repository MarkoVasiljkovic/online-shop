
<nav class="navbar sticky-top navbar-toggleable-md navbar-expand second-nav pb-1">
  {{-- <nav class="navbar second-navbar sticky-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav mt-5 black-bg"> --}}
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">

      <ul class="navbar-nav mx-auto sec-nav-text">
        
        <li class="nav-item pr-3">
        <a class="nav-link" style="font-size: 1.2rem;" href="/#my_carousel_1">HOME</a>
        </li>
        <li class="nav-item pr-3">
          <a class="nav-link" style="font-size: 1.2rem;" href="/#about">ABOUT US</a>
        </li>
        <li class="nav-item pr-3">
          <a class="nav-link" style="font-size: 1.2rem;" href="/#services">SERVICES</a>
        </li>
        <li class="nav-item pr-3">
          <a class="nav-link" style="font-size: 1.2rem;" href="/#projekts">OUR PROJECTS</a>
        </li>
        <li class="nav-item pr-3">
          <a class="nav-link" style="font-size: 1.2rem;" href="{{ route('shop.index')}}">OUR PRODUCTS</a>
        </li>
        <li class="nav-item pr-3">
          <a class="nav-link" style="font-size: 1.2rem;" href="/#contact">CONTACT</a>
        </li>
        
      </ul>
    </div>
  </nav>


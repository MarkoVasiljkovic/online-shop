@extends('layouts.admin')

@section('adminContent')
  @if( session('success'))
      <div class="alert alert-success">{{ session('success') }}</div>
  @endif
                    {{-- PRODUCTS --}}
          <h2 style="color: black;">Products</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Description</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                @foreach( $products as $item )
                <tr  style="border-bottom: solid 2px rgb(253, 0, 0);">
                    <th scope="row">{{ $item->id }}</th>
                    <td>{{ $item->name }}</td>
                    <td>                     
                        <img src="{{ route('product.outputimg','big_'.$item->image) }}" width="150">
                    </td>
                    <td>{{ $item->description }}</td>
                    <td>€{{ $item->price }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          {{--                             
          <hr>
          <h1>Colors</h1>

          <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Name</th>
                </tr>
              </thead>
              <tbody>
                  
                  @foreach( $color as $item )

                  <tr>
                      <th scope="row">{{ $item->id }}</th>
                      <td>{{ $item->name }}</td>
                    </tr>
                    @endforeach
                </tbody>
              </table> --}}
        </main>
      </div>
    </div>
@endsection
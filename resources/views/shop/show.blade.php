@extends('layouts.app')

@section('content')

<div class="container card card-body shadow mt-5 mb-5 col-7 bg-white">

  {{-- CAROUSEL --}}

  <div id="carouselExampleControls" class="carousel slide pb-5 " data-ride="carousel">
    <div class="carousel-inner">
      @foreach ($products as $item)
      <div class="carousel-item active">
        <img src="{{ route('product.outputimg','big_'.$item->image) }}" height="400" class="d-block w-100" alt="...">
      </div>
      @endforeach
      @foreach ($carusel as $image)
      <div class="carousel-item">
        <img src="{{ route('product.outputimg','big_'.$image->filename) }}" height="400" class="d-block w-100" alt="...">
      </div>
      @endforeach
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
    {{-- PRODUCT DETAILS --}}

    <div class="row justify-content-center">
          <div class="col-md-6">  

            @foreach ($products as $item)

            <div class="col-12 text-center">
              {{-- <img src="{{asset( 'storage/'.$item->draft_img )}}" class="rounded"> --}}
              <img src="{{ route('product.outputimg','big_'.$item->draft_img) }}" width="300" class="rounded">
            </div>    
          </div>
          <div class="col-md-6">
            <div class="row">   
            
            <div class="col-md-6 text-dark"> 
                <h5>{{ $item->name }}</h5>
                
                <p><span class="mr-1">Price: <strong>€{{ $item->price }}</strong></span></p>
                
                <p class="pt-1">{{ $item->description }}</p>
            </div>
              
            <div class="col-md-6 float-right">
              <img src="{{asset('images/ral_karta_obradjena.png')}}" height="250" class="d-block w-100" alt="...">
            </div>
          </div>
          <hr>
            
          <div class="container">
            <form action="{{ route('cart.add', $item->id) }}" method="GET">
                <div class="row">
                  <div class="form-group col-sm">
                    <label for="color">Color:</label>
                    <select class="form-control" name="color" id="color" type="text">
                        <option disabled selected> -- select color -- </option> 

                      @foreach ($color as $value)
                          <option value="{{ $value->name }}">{{ $value->name }}</option>
                      @endforeach

                    </select>
                  </div>
                  @can('customeronly')
                  <div class="col-sm">
                    <button type="submit" class="btn btn-primary btn-md mt-4 ml-5">
                      <i class="fa fa-cart"></i> Add to cart
                    </button>
                </div>
                @endcan
              </div>                
            </div>
            @endforeach
        </div>
  </div>
</div>
@endsection
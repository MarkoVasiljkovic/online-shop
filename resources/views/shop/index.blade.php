@extends('layouts.app')

@section('content')

<div class="container mt-5">

  <article class="row single-post no-gutters justify-content-center  gutter-sistem">
    <div class="col-md-9">
        <div class="image-wrapper float-left pr-3">
            <img src="images/sistem_montaze_oluka.png" width="650" height="550"  alt="">
        </div>
        <div class="single-post-content-wrapper p-3 text-white elements-text chango">
          <p>List of elements of typical gutter system:</p>
          <ol>
            <li>Gutter</li>
            <li>Gutter Bracket</li>
            <li>Gutter Corner</li>
            <li>Gutter outlet</li>
            <li>Elbow</li>
            <li>Clout</li>
            <li>Pipe</li>
            <li>End cap</li>
            </ol>
        </div>
    </div>
</article>

  <div class="row row-cols-3 justify-content-center text-dark">
    @foreach( $products as $item )


        <div class="col card m-5 card-body shadow">
          <img src="{{ route('product.outputimg','big_'.$item->image) }}" width="150" class="card-img-top" alt="product image">
          <div class="card-body">
            <h5 class="card-title">{{ $item->name }}</h5>
            <p class="card-text">{{ $item->description }}</p>
          <a href="{{ route('shop.show', $item->id)}}" class="btn btn-primary">Go to Product</a>
          </div>
        </div>

        @endforeach
      </div>
    </div>


@endsection
@extends('layouts.app')


@section('content')
{{-- @include('layouts.header') --}}

  @if( session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
@endif
<div class="row">

    {{-- Carousel 1 --}}

    <div id="my_carousel_1" class="carousel slide carousel-fade pb-5 col-6" data-ride="carousel" data-interval="5000">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="{{ asset('/images/firma_4.jpg') }}" height="400" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="{{ asset('/images/firma_3.jpg') }}" height="400" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="{{ asset('/images/firma_5.jpg') }}" height="400" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="{{ asset('/images/firma_2.jpg') }}" height="400" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="{{ asset('/images/firma_1.jpg') }}" height="400" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="{{ asset('/images/firma_7.jpg') }}" height="400" class="d-block w-100" alt="...">
        </div>
      </div>
    </div>

    {{-- Carousel 2 --}}

    <div id="my_carousel_2" class="carousel slide carousel-fade pb-5 col-6" data-ride="carousel" data-interval="5000">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="{{ asset('/images/elbows.jpg') }}" height="400" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="{{ asset('/images/corners.jpg') }}" height="400" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="{{ asset('/images/bracket.jpg') }}" height="400" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="{{ asset('/images/caps.jpg') }}" height="400" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="{{ asset('/images/snowguards.jpg') }}" height="400" class="d-block w-100" alt="...">
        </div>
      </div>
    </div>
</div>

<!-- Container (About Section) -->
  <div id="about" class="about-text">
    <div class="">

      <img src="{{asset('images/logo.png')}}" alt="logo" width="350px" class="rounded-circle float-right p-1">
      
      <p class="p-3 opc">
        Since its establishment, back in 1990, till today, LIMOMONTSTEEL successfully implements its main mission which reflects on high quality service and innovative solutions in construction sector of industry.
        
        Our strategy is based on key values we strive from the early beginning such as trust, reliability and responsibility both to the customers and employees but to the wider community too.
        
        We constantly invest in improving the quality and safety of our products and services. Also we tend to promote corporate responsibility and to raise awareness among others on how investing in innovations is important so base on that facts we actually are positive example which can be strived as a standard.
        
        Besides our leading position in construction industry, our vision is to be a synonym for company which successfully achieves its highly set standards. Having in mind all mentioned we are completely ready for new challenges and new markets.
        
        We have done a lot but work is ahead…
      </p>
    </div>
  </div>


  <!-- Container (Services Section) -->
<div id="services" class="container-fluid text-center text-dark fredoka">
    <h2 style="font-size: 5rem; color:black;">SERVICES</h2><br>
  <h4 style="color:black; font-size: 2rem; margin-bottom: 3rem;">What we offer</h4>

  <div class="row slideanim">

    <div class="col-sm-6">
      <div class="card text-white">
        <img class="card-img" src="{{ asset('images/steel_structures.jpg') }}" height="275" alt="Card image">
        <div class="card-img-overlay d-flex align-items-center justify-content-center">
          <h5 style="font-size: 2rem;" class="card-title black-bg">STEEL STRUCTURES ASSEMBLY</h5>
        </div>
      </div>
    </div>

    <div class="col-sm-6">
      <div class="card text-white">
        <img class="card-img" src="{{ asset('images/facade_panels.jpg') }}" height="275" alt="Card image">
        <div class="card-img-overlay d-flex align-items-center justify-content-center bg-black">
          <h5 style="font-size: 2rem;" class="card-title black-bg">INSTALLATION OF FACADE PANELS</h5>
        </div>
      </div>
    </div>

  </div>
  <br><br>

  <div class="row slideanim">
    
    <div class="col-sm-6">
      <div class="card text-white">
        <img class="card-img" src="{{ asset('images/roof_tiles.jpg') }}" height="275" alt="Card image">
        <div class="card-img-overlay d-flex align-items-center justify-content-center">
          <h5 style="font-size: 2rem;" class="card-title black-bg">PRODUCTION OF IMITATION ROOF TILES</h5>
        </div>
      </div>
    </div>

    <div class="col-sm-6">
      <div class="card text-white">
        <img class="card-img" src="{{ asset('images/currugated_sheet.jpg') }}" height="275" alt="Card image">
        <div class="card-img-overlay d-flex align-items-center justify-content-center">
          <h5 style="font-size: 2rem;" class="card-title black-bg">RODUCTION OF CORRUGATED SHEETS</h5>
        </div>
      </div>
    </div>

  </div>
</div>
    <!-- Container (Projects Section) -->
    <div id="projekts"  class="container-fluid text-center mt-5 text-black fredoka bg-black">
      <h2 style="font-size: 5rem; color:black;">Projects</h2><br>
      <h4 style="color:black; font-size: 2rem;">What we have created</h4>
    </div>
  <div class="container-fluid d-flex align-items-center justify-content-center mt-5 project-text {{-- fredoka --}} p-5">
    <div class="row text-center">
      <div class="col-sm-4">
        <div class="card" style="width: 18rem; opacity: 1;">
          <img class="card-img-top img-border" src="{{ asset('images/mladost.jpg') }}" alt="mladost">
          <div class="card-body">
            <h5 class="card-title">Leskovac</h5>
            <p class="card-text">Tile factory <br>"Mladost"</p>
            <a href="mladost" class="my-button">Go to gallery</a>
          </div>
        </div>
      </div> 
      <div class="col-sm-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top img-border" src="{{ asset('images/capriolo.jpg') }}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Backa Palanka</h5>
            <p class="card-text">Bicycle factory <br>"Capriolo"</p>
            <a href="capriolo" class="my-button">Go to gallery</a>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top img-border" src="{{ asset('images/eldisy.jpg') }}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Cacak</h5>
            <p class="card-text">Auto parts factory <br>"Eldisy"</p>
            <a href="eldisy" class="my-button">Go to gallery</a>
          </div>
        </div>
      </div>
    </div><br>
  </div>
@endsection

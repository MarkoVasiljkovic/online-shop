@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="card card-body bg-light shadow col-8 aling-center">
            <h1>Orders details</h1>


        <table class="table table-striped">
            <thead>
                <tr>
                        <th scope="row">Customer:</th>
                        <th>{{ $order->fullname }}</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope="row">ID:</td>
                        <td>{{ $order->id }}</td>
                    </tr>
                    <tr>
                        <td scope="row">Status:</td>
                        <td>{{ $order->status }}</td>
                    </tr>
                    <tr>
                        <td scope="row">Delivery method:</td>
                        <td>{{ $order->delivery_method }}</td>
                    </tr>
                    <tr>
                        <td scope="row">Total:</td>
                        <td>€ {{ $order->grand_total }}</td>
                    </tr>
                    <tr>
                        <td scope="row">Created at:</td>
                        <td>{{ $order->created_at }}</td>
                    </tr>
                    <tr>
                        <td scope="row">Item count:</td>
                        <td>{{ $order->item_count }}</td>
                        </tr>
                        <tr>
                            <td scope="row">State:</td>
                            <td>{{ $order->state }}</td>
                        </tr>
                        <tr>
                        <td scope="row">City:</td>
                        <td>{{ $order->city }}</td>
                        </tr>
                    <tr>
                        <td scope="row">Address:</td>
                        <td>{{ $order->address }}</td>
                        </tr>
                    <tr>
                        <td scope="row">Zipcode:</td>
                        <td>{{ $order->zipcode }}</td>
                        </tr>
                        <tr>
                            <td scope="row">Phone:</td>
                            <td>{{ $order->phone }}</td>
                        </tr>
                        <tr>
                            <td scope="row">Notes:</td>
                            <td>{{ $order->notes }}</td>
                        </tr>
                </tbody>
        </table>

        <h2>Items</h2>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Color</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orderItems as $item)
                        @foreach ($item->items as $item)
                            <tr >
                                <td scope="row">
                                    {{ $item->name }}
                                </td>
                                <td>
                                    <img src="{{ route('product.outputimg','prev_'.$item->image) }}">
                                </td>
                                <td>
                                    {{ $item->pivot->color }}
                                </td>
                                <td>€
                                    {{ $item->price }}
                                </td>
                                <td>
                                    {{ $item->pivot->quantity }}
                                </td>

                            </tr>
                        @endforeach
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
@endsection


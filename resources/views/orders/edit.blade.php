@extends('layouts.admin')

@section('adminContent')

@if( session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
@endif

<h1>Edit Order</h1>

<div id="form" class="form">
    <form action="{{ route('order.update',$order->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
          <label for="fullname">Fullname</label>
          <input type="text" class="form-control" name="fullname" id="fullname" value="{{ old('fullname',$order->fullname) }}">
          @error('fullname')
            <div class="text-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="state">State</label>
          <input class="form-control" id="state" name="state" rows="3" value="{{ old('state',$order->state) }}">
          @error('state')
            <div class="text-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="city">City</label>
          <input type="text" class="form-control" name="city" id="city" value="{{ old('city',$order->city) }}">
          @error('city')
            <div class="text-danger">{{ $message }}</div>
          @enderror
        </div>   
        <div class="form-group">
          <label for="address">Address</label>
          <input type="text" class="form-control" name="address" id="address" value="{{ old('address',$order->address) }}">
          @error('address')
            <div class="text-danger">{{ $message }}</div>
          @enderror
        </div>   
        <div class="form-group">
          <label for="zipcode">Postcode</label>
          <input type="text" class="form-control" name="zipcode" id="zipcode" value="{{ old('zipcode',$order->zipcode) }}">
          @error('zipcode')
            <div class="text-danger">{{ $message }}</div>
          @enderror
        </div>   
        <div class="form-group">
          <label for="phone">Phone</label>
          <input type="text" class="form-control" name="phone" id="phone" value="{{ old('phone',$order->phone) }}">
          @error('phone')
            <div class="text-danger">{{ $message }}</div>
          @enderror
        </div>  
        <div class="form-group">
            <label for="notes">Notes:</label>
            <textarea class="form-control" name="notes" id="notes" rows="3" value="{{ old('notes',$order->notes) }}"></textarea>
        </div>  
        <div class="form-group">
            <label for="delivery_method">Delivery method:</label>
            <select class="form-control" name="delivery_method" id="delivery_method" type="text">
                <option selected disabled>{{ old('delivery_method',$order->delivery_method) }}</option>
                <option>home delivery</option>
                <option>own transport</option>
            </select>
        </div>
        <div class="form-group">
            <label for="status">Status:</label>
            <select class="form-control" name="status" id="status" type="text">
                <option selected disabled>{{ old('status',$order->status) }}</option>
                <option>pending</option>
                <option>processing</option>
                <option>completed</option>
                <option>decline</option>
            </select>
        </div>
        
       
        <button type="submit" class="btn btn-dark">Submit</button>
    </form>
</div>


@endsection
        
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carusel extends Model
{
    use HasFactory;

    protected $fillable = ['filename', 'product_id'];

    public function product(){
        return $this->belongsTo(\App\Models\Product::class,'product_id','id');
    }
}

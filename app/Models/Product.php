<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['name','description','image','draft_img','price'];

    public function carusel(){
        return $this->hasMany(\App\Models\Carusel::class,'product_id','id');
    }

    public function orders(){

        return $this->belongsToMany(\App\Models\Product::class, 'order_items', 'product_id', 'order_id');
    }

}

<?php

namespace App\Http\Controllers;

use App\Mail\OrderMail;
use App\Models\Order;
use App\Models\Product;
use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:adminonly', ['except' => ['store','orders','destroy','customerShow']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = Order::paginate(8);

        return view('orders.index', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fullname' => 'required|min:2|max:200',
            'state' => 'required|min:2|max:100',
            'city' => 'required|min:2|max:100',
            'address' => 'required|min:2|max:200',
            'zipcode' => 'required|regex:/^[0-9]{3,7}$/',
            'phone' => 'required|min:5|max:30',
            'notes' => 'nullable'
        ]);

        $totalPrice = \Cart::session(auth()->id())->getTotal();
        $itemCount = \Cart::session(auth()->id())->getContent()->count();

        $order = new Order();

        $order->order_number = uniqid('Nr-');

        $order->fullname = $request->input('fullname');
        $order->state = $request->input('state');
        $order->city = $request->input('city');
        $order->address = $request->input('address');
        $order->zipcode = $request->input('zipcode');
        $order->phone = $request->input('phone');
        $order->delivery_method = $request->input('delivery_method');
        $order->notes = $request->input('notes');
            
        $order->grand_total = $totalPrice;
        $order->item_count = $itemCount;

        $order->user_id = auth()->id();

        $order->save(); 

        // save Order items in Pivot table
        $cartItems = \Cart::session(auth()->id())->getContent();
        

        foreach ($cartItems as $item) {
            $order->items()->attach($item->id, ['price' => $item->price, 'quantity' => $item->quantity, 'color' => $item->attributes->color]);
        }

        // empty cart
        \Cart::session(auth()->id())->clear();

        // send e-mail to admin

        $email = 'vasiljkovic.j5@gmail.com';

        $details = [
            'fullname' => $request->input('fullname'),
            'delivery_method' => $request->input('delivery_method'),
            'total'  => $totalPrice,
            'item_count' => $itemCount,
            'state' => $request->input('state'),
            'city' => $request->input('city'),
            'address' => $request->input('address'),
            'zipcode' => $request->input('zipcode'),
            'phone' => $request->input('phone'),
            'notes' => $request->input('notes')
        ];

        Mail::to($email)->send(new OrderMail($details));

        // take user to home page

        return redirect()->route('home')->with('success', 'Order successfully sent, thanks for the order!');

        


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $orderItems = Order::with('items')->where('id',$order->id)->get();

        return view('orders.show',compact('order','orderItems'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return view('orders.edit',compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $request->validate([
            'fullname' => 'required|min:2|max:200',
            'state' => 'required|min:2|max:100',
            'city' => 'required|min:2|max:100',
            'address' => 'required|min:2|max:200',
            'zipcode' => 'required|regex:/^[0-9]{3,7}$/',
            'phone' => 'required|min:5|max:30',
            'notes' => 'nullable'
        ]);

        //Array befüllen
        $requestUpdate = $request->all();


        $order->update($requestUpdate);

        return redirect()->route('order.index')->with('success','The order was changed');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        // dd($order);
        $order->delete();
        // 
        if( request()->ajax()){
            return response()->json(['status'=>200]);
        }

        if(Auth::user()->isAdmin()){

            return redirect()->route('order.index')->with('success','The order was deleted!');
        }
        else{

            return redirect()->route('order.user',Auth::user()->id)->with('success','The order was deleted!');
        }
    }

    /**
     * Display a listing of the resource for custumer.
     *
     * @return \Illuminate\Http\Response
     */
    public function orders($id)
    {
        $order = Order::where('user_id',$id)->paginate(7);

        return view('cart.orders', compact('order'));
    }

        /**
     * Display the specified resource for custumer.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function customerShow(Order $order)
    {
        $orderItems = Order::with('items')->where('id',$order->id)->get();

        // dd($orderItems);

        return view('orders.customer_show',compact('order','orderItems'));
    }



}

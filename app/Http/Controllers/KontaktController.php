<?php

namespace App\Http\Controllers;

use App\Mail\KontaktMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class KontaktController extends Controller
{


    public function store(Request $request){
        
        $request->validate([
            'name' =>'required|min:2',
            'email' => 'required|email:filter,dns',
            'msg' =>'required|min:10',
        ]);
        
        // Enter your address here
        $email = 'example@gmail.com';

        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'message'  => $request->input('msg')
        ];

        Mail::to($email)->send(new KontaktMail($data));

        return redirect()->route('home')->with('success', 'Message successfully sent');
    }

}

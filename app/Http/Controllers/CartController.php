<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Darryldecode\Cart\Facades\CartFacade;
 
class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'add']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cartItems = \Cart::session(auth()->id())->getContent();
        
        return view('cart.index', compact('cartItems'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $rowId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $rowId)
    {
        /* quantity update */
        \Cart::session(auth()->id())->update($rowId,[
            'quantity' => array(
                'relative' => false,
                'value' => request('quantity')
            ),
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $itemId
     * @return \Illuminate\Http\Response
     */
    public function destroy($itemId)
    {
        \Cart::session(auth()->id())->remove($itemId);
        
        return back();
    }

    /**
     * Add the specified product to Cart.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request, Product $product){
        $color = $request->input('color','test');
        // $color = $request->query('color');
        // $color = $request->color->name;
       
        
        // dd($color);
    
        // add the product to cart
        \Cart::session(auth()->id())->add(array(
            'id' => $product->id,
            'name' => $product->name,
            'price' => $product->price,
            'quantity' => 1,
            'attributes' => array('color' => $color),
            'associatedModel' => $product
        ));
    
        return redirect()->route('cart.index');
    }

    /**
     * Checkout
     */
    public function checkout(){
        
        return view('cart.checkout');
    }
}

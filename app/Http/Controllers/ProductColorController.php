<?php

namespace App\Http\Controllers;

use App\Models\ProductColor;
use Illuminate\Http\Request;

class ProductColorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $color = ProductColor::get();
        return view('product_color.index',compact('color'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'product_color.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|min:2|max:100',
        ]);

        $color = new ProductColor($request->all());
        $color->save();

        return redirect()->route('productColor.index')->with('success','Color saved');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductColor  $productColor
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductColor $productColor)
    {

        return view('product_color.edit',compact('productColor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductColor  $productColor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductColor $productColor)
    {
        $request->validate([
            'name'=>'required|max:100',
            ]);

        $nameOld = $productColor->name;
        
        $productColor->update($request->all());
        
        return redirect()->route('productColor.index')->with('success','The Color '.$nameOld.' was on '.$request->name.' changed!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductColor  $productColor
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductColor $productColor)
    {
        $productColor->delete();

         if( request()->ajax()){
             return response()->json(['status'=>200]);
         }
         return redirect()->route('productColor.index')->with('success','The Color was deleted!');
    }
    
}

<?php

namespace App\Http\Controllers;

use App\Models\Carusel;
use App\Models\Product;
use App\Models\ProductColor;


class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Getting the collection from the Model Product
        $products = Product::get();

        // Displaying content in browser from blade view file 'shop/index.blade.php' with compacted Product Model
        return view('shop.index',compact('products'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Getting the collection for selected product from the Model Product
        $products = Product::where('id',$id)->get();

        // Getting the multiple images for selected product from the Model Carusel
        $carusel = Carusel::where('product_id',$id)->get();

        // Getting colors for produkt from Model ProductColor
        $color = ProductColor::get();

        // Displaying content in browser from blade view file 'shop/show.blade.php' with compacted Models
        return view('shop.show',compact('products', 'carusel', 'color'));
    }

}

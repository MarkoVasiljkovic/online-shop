<?php

namespace App\Http\Controllers;

use App\Models\Carusel;
use App\Traits\UploadImage;
use App\Models\Product;
use App\Models\ProductColor;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('restrictToAdmin', ['except' => ['outputimg','search']]);
    }
    
     /**
     * Trait Image Upload resize max. Breite 800 und Quadrat 150px
     */
    use UploadImage;

    /**@var array Form input name*/
    protected $inputFile = [];
    /**@var string config/filesystems.php disks->index */
    protected $diskName = 'public';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::get();

        return view('product.index',compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'product.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $request->validate([
            'name'=>'required|min:2|max:100',
            'description' =>'required|min:2',
            'image' => 'required|mimes:jpg,jpeg,png,gif|max:12288',
            'draft_img' => 'required|mimes:jpg,jpeg,png,gif|max:12288',
            'price' => 'required',
            'filenames.*' => 'nullable|mimes:jpg,jpeg,png,gif|max:40000'
        ]);
        
        /* Product images */
        if( $request->hasFile('image') && $request->hasFile('draft_img')){ 

            $this->inputFiles = [$request->file('image'), $request->file('draft_img')];
            $fileNames = [];           
            try{
                foreach ($this->inputFiles as $inputFile) {
                    
                    $this->storeFile($inputFile);
                    $this->resizeMaxWidth($inputFile, 800, 'big_');
                    $this->square($inputFile, 150, 'prev_');
                    
                    array_push($fileNames, $this->traitOriginalFilename);
                }    
            }
            catch(\Exception $e){
                if( config('app.debug') ){
                    abort('580',$e->getMessage().' '.$e->getFile().' '.$e->getLine());
                }
                else{
                        abort('580','Server error, file cannot be saved!');
                }
            }
            
            $product = new Product($request->all());

            $product->image = $fileNames[0];
            $product->draft_img = $fileNames[1];
            $product->save();         
        }
            /* Carusel images */
        if($request->hasFile('filenames')){
            
            $files = $request->file('filenames');
            $images = [];

            try{
                foreach ($files as $file) {
                    
                    $this->storeFile($file);
                    $this->resizeMaxWidth($file, 800, 'big_');
                    // $this->square($file, 150, 'prev_');
                    
                    array_push($images, $this->traitOriginalFilename);
                }    
            }
            catch(\Exception $e){
                if( config('app.debug') ){
                    abort('580',$e->getMessage().' '.$e->getFile().' '.$e->getLine());
                }
                else{
                        abort('580','Server error, file cannot be saved!');
                    }
                }

            foreach ($images as $image) {
                $carusel = new Carusel([
                    'filename' => $image,
                    'product_id' => $product->id
                ]);
                $carusel->save();
            } 

        }

        return redirect()->route('admin.index')->with('success','Product saved');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        // Getting the multiple images for selected product from the Model Carusel
        $carusel = Carusel::where('product_id',$product->id)->get();

        // Getting colors for produkt from Model ProductColor
        $color = ProductColor::get();
        
        return view('product.show',compact('product', 'carusel', 'color'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('product.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name'=>'required|min:2|max:100',
            'description' =>'required|min:2',
            'image' => 'nullable|mimes:jpg,jpeg,png,gif|max:12288',
            'draft_img' => 'nullable|mimes:jpg,jpeg,png,gif|max:12288',
            'price' => 'required',
            'filenames' => 'nullable|max:12288'
        ]);
       

        //Array befüllen
        $requestUpdate = $request->except(['image', 'draft_img']);
        
        $product->update($requestUpdate);

        //Prüfen ob ein Bild hochgeladen wurde
        if($request->hasFile('image') && $request->hasFile('draft_img')){

            //Die Bilder aus dem input file holen
            $this->inputFiles = [$request->file('image'), $request->file('draft_img')];
            $fileNames = [];

            //Bilder löschen
            $this->dropImages($product->image,['big_','prev_']);
            $this->dropImages($product->draft_img,['big_','prev_']);

            try{
                // Bilder speichern
                foreach ($this->inputFiles as $inputFile) {
                    
                    $this->storeFile($inputFile);
                    $this->resizeMaxWidth($inputFile, 800, 'big_');
                    $this->square($inputFile, 150, 'prev_');

                    array_push($fileNames, $this->traitOriginalFilename);
                }    
            }
            catch(\Exception $e){
                if( config('app.debug') ){
                    abort('580',$e->getMessage().' '.$e->getFile().' '.$e->getLine());
                }
                else{
                    abort('580','Server error, file cannot be saved!');
                    }
                }
                // Datenbank Bilder name update
                $product->image = $fileNames[0];
                $product->draft_img = $fileNames[1];
                $product->update(['image', 'draft_img']);
                
        }
            /* Carusel images */
            if($request->hasFile('filenames')){

                // Getting the multiple images for selected product from the Model Carusel
                $carusel = Carusel::where('product_id',$product->id)->get();

                //Carusel Bilder löschen
                foreach($carusel as $img){
                    $this->dropImages($img->filename, ['big_', 'prev_']);
                }
                //Die Carusel Bilder aus dem input file holen
                $files = $request->file('filenames');
                $images = [];
    
                try{
                    // Carusel Bilder speichern
                    foreach ($files as $file) {
                        
                        $this->storeFile($file);
                        $this->resizeMaxWidth($file, 800, 'big_');
                        // $this->square($file, 150, 'prev_');
                        
                        array_push($images, $this->traitOriginalFilename);
                    }    
                }
                catch(\Exception $e){
                    if( config('app.debug') ){
                        abort('580',$e->getMessage().' '.$e->getFile().' '.$e->getLine());
                    }
                    else{
                            abort('580','Server error, file cannot be saved!');
                        }
                    }
                // Datenbank Carusel Bilder name update
                foreach ($images as $image) {
                    $carusel = new Carusel([
                        'filename' => $image,
                        'product_id' => $product->id
                    ]);
                    $carusel->save();
                } 
    
            }


        return redirect()->route('product.index')->with('success','The product was changed!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        // Getting the multiple images for selected product from the Model Carusel
        $carusel = Carusel::where('product_id',$product->id)->get();

        foreach($carusel as $img){

            $this->dropImages($img->filename, ['big_', 'prev_']);
        }

        $this->dropImages($product->image,['big_','prev_']);
        $this->dropImages($product->draft_img,['big_','prev_']);
        $product->delete();

        if( request()->ajax()){
            return response()->json(['status'=>200]);
        }
        return redirect()->route('product.index')->with('success','The product was deleted!');
    }
    /**
     * output Image
     */
    public function outputimg($image){
        return $this->traitFileOutput($image);
    }

    /**
     * Search Produkt
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request){

        $searchText = $request->input('searchText');
        $products = Product::where('name','Like','%'.$searchText.'%')->get();

        return view('product.search',compact('products'));
    }

}

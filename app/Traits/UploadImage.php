<?php

namespace App\Traits;

use Exception;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

trait UploadImage{

    protected $traitOriginalFilename;
    private $insertPath;
    protected $fileUrl;

    /**
     * Entfernt alle zeichen ausser a-Z0-9 und . aus dem Filename
     */
    protected function checkFile($inputFile){
        if( !isset($this->diskName ) ){
            throw new \Exception('kein Property diskName definiert!');
        }
        
            $originalFileName = preg_replace('![^a-z0-9\.]!i','_', $inputFile->getClientOriginalName());
            // dd($originalFileName);
            
            $fileExists = Storage::disk($this->diskName)->exists($originalFileName);
            if( $fileExists ){
                //$originalFileName = time().'_'.$originalFileName;
                $originalArray = pathinfo($originalFileName);
                $originalFileName = $originalArray['filename'].'_'.time().'.'. $originalArray['extension'];                
            }
            
            $this->traitOriginalFilename = $originalFileName;
        
    }

    /**
     * speichert die Original Datei ab
     */
    protected function storeFile($inputFile){
        
            $this->checkFile($inputFile);
        
       
        $inputFile->storeAs('',$this->traitOriginalFilename,$this->diskName);
        
    }

    /**
     * Verkleinert das Bild auf max. Breite
     * @param int $width 
     * @param mixed string/false [default false] prefix Dateiname
     */
    protected function resizeMaxWidth($inputFile, int $width,$prefix=false ){
        if( !$this->traitOriginalFilename){
            $this->checkFile($inputFile);
        }
        // dd($inputFile->getRealPath());
        $img = Image::make( $inputFile )->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $this->setPath();
        $img->save($this->insertPath.'/'.$prefix.$this->traitOriginalFilename);
        
    }

    /**
     * Verkleinert das Bild auf max. Breite
     * @param int $width 
     * @param mixed string/false [default false] prefix Dateiname
     */
    protected function square($inputFile, int $width,$prefix=false ){
        if( !$this->traitOriginalFilename){
            $this->checkFile($inputFile);
        }
       
        $img = Image::make( $inputFile )->fit($width);
        
        $this->setPath();
        $img->save($this->insertPath.'/'.$prefix.$this->traitOriginalFilename);
    }

    /**
     * Variablen aus config/filesystems.php ->disk
     */
    private function setPath(){
        if( !isset($this->diskName) ){
            throw new \Exception('kein Property inputFile oder diskName definiert!');
        }
        $this->insertPath = config('filesystems.disks.'.$this->diskName.'.root');
        $this->fileUrl = config('filesystems.disks.'.$this->diskName.'.url');
    }

    /**
     * Variablen aus config/filesystems.php ->disk
     * @return string $this->fileUrl
    */
    protected function getPath(){
        $this->setPath();
        return $this->fileUrl;
    }

    /**
     * Variablen aus config/filesystems.php ->disk
     * @return string $this->insertpath
    */
    protected function getServerPath(){
        $this->setPath();
        return $this->insertPath;
    }

    /**
     * @var string $filename
     * @return 
     */
    protected function traitFileOutput($filename){
        $file = $this->getServerPath().'/'.$filename;
        if(file_exists($file)){
            return response()->file($file);
        }
        return response('File not found',404);
    }

    /**
     * Dateien löschen
     * @param string $filename
     * @param mixed string/array/false
     */
    protected function dropImages($filename,$prefix=false){
      
        $files = [$filename];
        if( is_array($prefix)){
            foreach($prefix as $pre){
                $files[] = $pre.$filename;
            }
        }
        else if(is_string($prefix)){
            $files[] = $prefix.$filename;
        }
        Storage::disk($this->diskName)->delete($files);
        
    }
}
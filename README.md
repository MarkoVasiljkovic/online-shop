<!-- <p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p> -->

<p align="center">
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Funktionalität

1. Ein nicht registrierter Benutzer

        - kann das Unternehmen per E-Mail kontaktieren
        - den gesamten Inhalt der Seiten anzeigen.

2. Der registrierte Benutzer

        - kann das Unternehmen per E-Mail kontaktieren
        - den gesamten Inhalt der Seiten anzeigen.
        - kann Produkte in den Warenkorb legen
        - hat Möglichkeit Produkte zu bestellen
        - Alle Bestellinformationen anzeigen
        - kann sein Konto verwalten (Namens- und Passwortänderung)

3. Der Administrator

        - kann die Produkte verwalten (CRUD)
        - hat einen Überblick über alle Bestellungen
        - erhält eine E-Mail mit Informationen zu jeder Bestellung
        - kann den Status aller Bestellungen aktualisieren




## How to use

1. Clone repo

        git clone https://gitlab.com/MarkoVasiljkovic/online-shop.git

2. Install the composer dependencies

        composer install

3. Save .env.example as .env and put your database credentials

4. Set application key

        php artisan key:generate

5. And Migrate

        php artisan migrate



